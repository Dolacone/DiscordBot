from importlib import import_module


def dispatch(message, text):
    """handle command
    :param command:
    :return: output content to discord
    """
    text_parts = text.split(' ')
    text_parts = [element for element in text_parts if element != '']

    command = text_parts[0]
    arguments = text_parts[1:]

    print("players: {}".format(message.author))
    print("command: {}".format(command))
    print("arguments: {}".format(arguments))

    try:
        command_module = import_module("commands.{}".format(command))
    except ImportError:
        return "Invalid command: {}".format(command)

    result = command_module.main(arguments)
    if type(result) == list:
        return "\n".join(result)
    return result
