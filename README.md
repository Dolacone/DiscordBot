# DiscordBot Template

Quick template to build discord bot.

# Quick Start

## Create Bot Application

Create bot application here: https://discordapp.com/developers/applications/me

Remember client ID and Token for later usage.

## Invite Bot to Server

Visit the URL https://discordapp.com/oauth2/authorize?client_id=XXXXXXXXXXXX&scope=bot but replace XXXX with your app client ID.

## Start Bot

Update bot token in `bot.conf`.

Then start your bot with `python main.py`, and it should be available in your server.

You must tag (`@`) the bot to make it work, try to poke it with `@BotName example`.

# Add Commands

Create a python file under commands folder, name it with your command name, and it will be working like this:

```
(type in discord)
@BotName CommandName argument1 argument2 argument3
```

```
command = "CommandName"
arguments = ["argument1", "argument2", "argument3"]

from commands import CommandName
result = CommandName.main(arguments)
discord.print(result)
```

Sample code for commands can be found in [HERE](./commands/example.py).
