import configparser
import re
import discord

import handler

config = configparser.ConfigParser()
config.read('bot.conf')

TOKEN = config.get('bot', 'token')
USER = {}

client = discord.Client()


async def replace_mention_with_username(content):
    mention_list = re.findall('<@\!?\d*>', content)
    print(mention_list)
    result = content
    for mention_tag in mention_list:
        user_id = re.findall('\d+', mention_tag)[0]
        user_name = USER[user_id]
        result = result.replace(mention_tag, "<{}>".format(user_name))
    return result


@client.event
async def on_message(message):
    # take reaction only if bot is tagged in content
    command_prefix = '<@{}>'.format(client.user.id)
    if not message.content.startswith(command_prefix):
        return
    print("{}: {}: {}".format(message.channel, message.author, message.content))

    # avoid bot replies to itself
    if message.author == client.user:
        return

    text = message.content.lstrip(command_prefix).lstrip(' ')
    text = await replace_mention_with_username(text)

    response_message = ""
    try:
        response_message = handler.dispatch(message, text)
    except Exception as e:
        response_message = "ERROR: {}".format(e)
        raise
    finally:
        response = "{}\n{}".format(message.author.mention, response_message)
        print(response)
        await client.send_message(message.channel, response)


def cache_user_id():
    print('loading user list')
    members = client.get_all_members()
    for member in members:
        USER[member.id] = "{}#{}".format(member.name, member.discriminator)
    print('{} users loaded'.format(len(USER)))


@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    cache_user_id()
    print('------')

client.run(TOKEN)
